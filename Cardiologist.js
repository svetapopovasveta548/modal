import Visit from "./Visit.js";
import createName from "./check.js";




export default class VisitCardiologist extends Visit {
    constructor() {
        super()
 
    }

    createVisitTherapist() {
        this.div = document.createElement("div")
        this.inputage = createName("input", "age")
        this.inputpressure = createName("input", "pressure")
        this.inputindex = createName("input", "index")
        this.inputdiseases = createName("input", "diseases")
        this.inputage.placeholder = "age"  
        this.inputpressure.placeholder = "normal pressure"  
        this.inputindex.placeholder = "body mass index"  
        this.inputdiseases.placeholder = "past diseases of the cardiovascular system" 
        this.div.append(this.inputage, this.inputindex, this.inputpressure, this.inputdiseases)
        return this.div
    }


    render() {
        const select = new Visit().renderSelect()
        const b = this.createVisitTherapist()
        this.form.append(this.inputFullName, this.inputPurpose, this.inputDescription, this.select, b, this.btn)
        return this.form
    }
}