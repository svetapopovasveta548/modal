export default function createName(tagName, Name) {
    const element = document.createElement(tagName);
    element.name = Name;
    return element;
}