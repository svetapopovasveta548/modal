import createElement from "./functions.js";
import createName from "./check.js";



export default class Visit {
    constructor() {
        this.inputFullName = createName("input", "fname")
        this.inputFullName.placeholder = "full name";
        this.inputPurpose = createName("input", "purpose");
        this.inputPurpose.placeholder = "purpose of the visit";
        this.inputDescription = createName("input", "desc");
        this.inputDescription.placeholder = "brief description of the visit";
        this.form = createElement("form", "form-data", "form-data")
        this.form.name = "myForm";
        this.btn = document.createElement("button");
        this.select = this.renderSelect()
        this.btn.addEventListener("click", (e) => {
            e.preventDefault()
            let validation = true
            const forms = [...document.forms["myForm"]].forEach(el => {
                if (!el.value && el !== document.forms["myForm"].querySelector("button")) {
                    el.style.background = "red"
                    validation = false
                }
            })
            if (validation) {
                const object = Object.fromEntries(new FormData(document.forms["myForm"]))
                console.log(object)
            }


        })
    }



    renderSelect() {
        const dropdownField = ["normal", "priority", "urgent"];
        this.select = document.createElement("select");
        this.select.name = "select";
        this.optionHidden = document.createElement("option");
        this.optionHidden.text = "Urgency of the visit";
        this.optionHidden.value = "";
        this.optionHidden.hidden = true;
        this.select.add(this.optionHidden);
        dropdownField.forEach(el => {
            const option = document.createElement("option");
            option.value = el;
            option.text = el;
            option.id = "option";
            this.select.add(option);
        })
        return this.select
    }

}