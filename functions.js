export default function createElement(tagName, className, idName) {
    const element = document.createElement(tagName);
    element.className = className;
    element.id = idName;
    return element;
}




